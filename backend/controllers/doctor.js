var randomstring = require("randomstring");
var Sequelize = require("sequelize");
var Op = Sequelize.Op;
var sequelize = require("../models/model.js").sequelize;
var Model = require("../models/model.js").Model;
var config = require("../config.json");
var fs = require("fs");
var doctor = (module.exports = {});

doctor.addMedicationPlan = function(req, res, next) {
  const patientId = req.body.user_id;
  const medplan = req.body.medplan;
  let starting_date = new Date(req.body.starting_date)
  let formatedStarting_date = new Date (starting_date.getFullYear(),starting_date.getMonth(),starting_date.getDay())
  let ending_date = new Date(req.body.starting_date)
  let formatedEnding_date = new Date (ending_date.getFullYear(),ending_date.getMonth(),ending_date.getDay())
  var medicationPlan = []

  for(let item of medplan){
    let a = {
      medications:item.medication.id,
      intake_interval:item.interval
    }
    medicationPlan.push(a)
  }
 
  //Patient has medical plan through MedicationPlan
  //Medication plan has items through MedPlanItem

  Model.Patient.findOne({
    where: {
      user_id: patientId
    }
  }).then(patient => {
    Model.MedicationPlan.create(
      {
        starting_date: formatedStarting_date,
        ending_date: formatedEnding_date,
        items: medicationPlan
      },
      {
        include: [
          {
            model: Model.MedPlanItem,
            as: "items"
          }
        ]
      }
    ).then(medplan => {
      medplan.setPatient(patient).then(response => {
        console.log(response);
        res.status(200).end();
      });
    });
  });
};

doctor.getMedicationPlan = function(req, res, next) {
  const patient = req.query.patientId;

  Model.Patient.findOne({
    where: {
      user_id: patient
    }
  }).then(pat => {
    pat
      .getMedical_records({
        include: [
          {
            model: Model.MedPlanItem,
            as: "items",
            include: [
              {
                model: Model.Medication,
                target: "medications"
              }
            ]
          }
        ]
      })
      .then(response => {
        res.status(200).send(response);

      });
  });
};
