var randomstring = require("randomstring");
var Sequelize = require("sequelize");
var Op = Sequelize.Op;
var sequelize = require("../models/model.js").sequelize;
var Model = require("../models/model.js").Model;
var config = require("../config.json");
var fs = require("fs");
var caregiver = (module.exports = {});

caregiver.post = function(caregiver_id, patient_id) {
  Model.caregiver.create({
    user_id: caregiver_id,
    assigned_patients: patient_id
  });
};


caregiver.assign = function(req, res, next) {
  const caregiver = req.body.caregiverId;
  const caregivers_list = req.body.caregivers_list;
  const payload = [];
  for (let i of caregivers_list) {
    payload.push({ user_id: i });
  }
  Model.Caregiver.findOne({
    where: {
      user_id: caregiver
    }
  }).then(user => {
    Model.Patient.findAll({ where: { [Op.or]: payload } }).then(patients => {
      user.setPatients(patients).then(rs => {
        if (rs) {
          res.status(200).end();
        }
        else
        res.status(400).end();
      });
    });
  });
};

caregiver.get_assigned_patients = function (req,res,next) {

    Model.Caregiver.findOne({
        where: {
          user_id: req.query.caregiver
        }
      }).then(result=>{
          result.getPatients({
            include: [
              {
                model: Model.User,
                attributes: [
                  "id",
                  "username",
                  "name",
                  "birth_date",
                  "gender",
                  "address",
                  "role"
                ]
              }
            ]
          }).then(patients=>{
             res.status(200).send(patients)
          })
      })
}

caregiver.get = function(req, res, next) {
  Model.Caregiver.findAll({
    include: [
      {
        model: Model.User,
        attributes: [
          "id",
          "username",
          "name",
          "birth_date",
          "gender",
          "address",
          "role"
        ]
      }
    ]
  }).then(result => {
    res.status(200);
    res.send(result);
  });
};
