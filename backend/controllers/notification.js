var Sequelize = require('sequelize')
var sequelize = require('../models/model.js').sequelize
var Model = require('../models/model.js').Model
var config = require('../config.json')
var notification = (module.exports = {})
var Op = Sequelize.Op

notification.get_new_notifications = function (req, res, next) {
  const user = req.sessionUser
  Model.Notification.findAll({
    where: { userId: user.id, seen: false, sent: false }
  }).then(result => {
    if (result) {
      const query = result.map(element => {
        return element.id
      })

      Model.Notification.update(
        { sent: true },
        { where: { id: { [Op.or]: query } } }
      ).then(resp => {
        res.status(200).send(result)
      })
    }
  })
}

notification.add_notification = function (notification, user) {
  Model.Notification.create({
    message: notification,
    seen: false,
    sent: false
  }).then(notification_obj => {
    notification_obj.setUser(user).then(result => {
      console.log(result)
    })
  })
}
