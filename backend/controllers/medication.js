var Sequelize = require("sequelize");
var sequelize = require("../models/model.js").sequelize;
var Model = require("../models/model.js").Model;
var config = require("../config.json");
var medication = (module.exports = {});

medication.post = function(req, res, next) {
  
  const payload = {
    name: req.body.name,
    dosage: req.body.dosage,
    side_effects_list: req.body.side_effects_list
  };
  Model.Medication.create(payload).then(result => {
    res.status(200).send(result);
});
};

medication.get = function(req, res, next) {
  Model.Medication.findAll({
    attributes : ['id','name','dosage','side_effects_list']
  }).then(result => {
    if (result)
    res.status(200).send(result);
});
};
