var randomstring = require("randomstring");
var Sequelize = require('sequelize');
var Op = Sequelize.Op;
var sequelize = require('../models/model.js').sequelize;
var Model = require('../models/model.js').Model;
var config = require('../config.json');
var fs = require('fs');
var user = module.exports = {};

user.change_password = function (req, res, next) {
    if( req.sessionUser.updatePassword(req.body.currentpassword , req.body.newpassword) ) {
        res.status(200).end();
    } else {
        return next("Old password is incorrect");
    }
}

user.login= function (req, res, next) {
    var username = req.body.username;
    var password = req.body.password;
        Model.User.findOne({ where: { username: username } }).then(function (user) {
            if (!user) {
                res.status(401);
                res.end( `{"message":"invalid user"}` );
            } else if (!user.validPassword(password)) {
                res.status(401);
                res.end( `{"message":"invalid password"}` );
            } else {
                var token = randomstring.generate(60);
                user.setToken(token);
                res.status(200);
                res.end( `{"message":"succes","token":"${token}", "role":"${user.role}"}` );
            }
        });
    
}

user.check_doctor = function (req, res, next) {
    res.status(200).send();
}

user.check_login = function (req, res, next) {
    res.status(200).send(req.sessionUser.user === 1);
}

user.check_caregiver = function (req, res, next) {
    res.status(200).send(req.sessionUser.role === 2);
}

user.get_user_info = function(req, res, next) {
  res.status(200).send(req.sessionUser);
}

user.get = function (req, res, next) {
    Model.Patient.findAll({
        include:[{model:Model.User,attributes: ['id', 'username', 'name', 'birth_date', 'gender', 'address', 'role']}]
    }).then(result => {
        res.status(200);
        res.send(result);
    });
}


user.getFree = function (req, res, next) {
    Model.Patient.findAll({
        where: { caregiverId: null },
        include:[{model:Model.User,attributes: ['id', 'username', 'name', 'birth_date', 'gender', 'address', 'role']}]
    }).then(result => {
        res.status(200);
        res.send(result);
    });
}

user.getCaregivers = function (req, res, next) {
    Model.Patient.findAll({
        include:[{model:Model.User,attributes: ['id', 'username', 'name', 'birth_date', 'gender', 'address', 'role']}]
    }).then(result => {
        res.status(200);
        res.send(result);
    });
}

user.update_get = function (req, res, next) {
    Model.User.findAll({
        attributes: ['id','username', 'name', 'address', 'role', 'birth_date'],
        where: { username: req.sessionUser.username }
    }).then(result => {
        res.status(200).send(result);
    });
}
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-'); /// day and month is swaped because US format
}

user.post = function (req, res, next) {
    let date = new Date(req.body.user.birth_date)
    let payload  = req.body.user
    let formatedDate = new Date (date.getFullYear(),date.getMonth(),date.getDay())
    console.log(formatedDate)
    Model.User.create({
        username: payload.username,
        name: payload.name,
        birth_date: formatedDate,
        gender : payload.gender,
        address : payload.address,
        password: payload.password,
        role: payload.role
    })
    .then(user => {
        const role = user.role;
        switch(role){
            case 0:
                Model.Patient.create({
                    user_id:user.id
                })
            break;
            case 1: 
                Model.Doctor.create({
                    user_id:user.id
                })
                break;
                case 2: 
                Model.Caregiver.create({
                    user_id:user.id
                })
        }
        res.status(200);
        res.end();
    });
}

user.update = function (req, res, next) {
    let date = req.body.birth_date;
    let fm = date.split('-')
    let dbFormat = new Date (fm[2],fm[0],fm[1])
    Model.User.update({
        name: req.body.name,
        birth_date: dbFormat,
        gender : req.body.gender,
        address : req.body.address,
    })
    .then(user => {
        res.status(200);
        res.end();
    });
}

user.delete = function (req, res, next) {
    var username = req.body.username;
    Model.User.destroy({
        where: {
            username: username
        }
    }).then(result => {
        res.status(200);
        res.end();
    });
}
