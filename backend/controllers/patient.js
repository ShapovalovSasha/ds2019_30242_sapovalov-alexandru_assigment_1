var randomstring = require('randomstring')
var Sequelize = require('sequelize')
var Op = Sequelize.Op
var sequelize = require('../models/model.js').sequelize
var Model = require('../models/model.js').Model
var config = require('../config.json')
var fs = require('fs')
var patient = (module.exports = {})

patient.saveActivity = function (req, next) {
  const activity = JSON.parse(req.body.activity)
  const patient = req.patient
  return new Promise((resolve, reject) => {
    Model.Activity.create({
      activity_label: activity.activity,
      starting_date: activity.start,
      ending_date: activity.end
    }).then(act => {
      act.setPatient(patient).then(res => {
        resolve(res)
      })
    })
  })
}
