var sequelize = require('../model.js').sequelize;
var Sequelize = require('sequelize');

var MedicationPlan = module.exports = sequelize.define('medplans', {
    starting_date: {
        type: Sequelize.DATE,
        unique: false,
        allowNull: false
    },
    ending_date: {
        type: Sequelize.DATE,
        unique: false,
        allowNull: false
    }
});
