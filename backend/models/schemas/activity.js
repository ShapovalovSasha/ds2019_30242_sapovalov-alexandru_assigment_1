var sequelize = require('../model.js').sequelize
var Sequelize = require('sequelize')

var Activity = (module.exports = sequelize.define('activities', {
  activity_label: {
    type: Sequelize.STRING,
    unique: false,
    allowNull: false
  },
  starting_date: {
    type: Sequelize.DATE,
    unique: false,
    allowNull: false
  },
  ending_date: {
    type: Sequelize.DATE,
    unique: false,
    allowNull: false
  }
}))
