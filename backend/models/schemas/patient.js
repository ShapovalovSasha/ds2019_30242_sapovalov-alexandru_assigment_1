var sequelize = require('../model.js').sequelize;
var Sequelize = require('sequelize');

var Patient = module.exports = sequelize.define('patients', {
    user_id: {
        type: Sequelize.INTEGER,
        unique: false,
        allowNull: false
    },
    medical_record: {
        type: Sequelize.INTEGER,
        unique: false,
        allowNull: true
    }
});
