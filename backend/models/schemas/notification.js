var sequelize = require('../model.js').sequelize
var Sequelize = require('sequelize')

var Notification = (module.exports = sequelize.define('notifications', {
  message: {
    type: Sequelize.STRING,
    unique: false,
    allowNull: false
  },
  seen: {
    type: Sequelize.BOOLEAN,
    unique: false,
    allowNull: false
  },
  sent: {
    type: Sequelize.BOOLEAN,
    unique: false,
    allowNull: false
  }
}))
