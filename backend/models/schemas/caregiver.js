var sequelize = require('../model.js').sequelize;
var Sequelize = require('sequelize');

var Caregiver = module.exports = sequelize.define('caregivers', {
  
    user_id: {
        type: Sequelize.INTEGER,
        unique: false,
        allowNull: false
    },
    assigned_patients:{
        type: Sequelize.INTEGER,
        unique: false,
        allowNull: true
    }

});
