var sequelize = require('../model.js').sequelize;
var Sequelize = require('sequelize');

var MedPlanItem = module.exports = sequelize.define('planitems', {
    intake_interval: {
        type: Sequelize.STRING,
        unique: false,
        allowNull: false
    },
    // medications: {
    //     type: Sequelize.INTEGER,
    //     unique: false,
    //     allowNull: false
    // }
});
