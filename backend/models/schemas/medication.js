var sequelize = require('../model.js').sequelize;
var Sequelize = require('sequelize');

var Medication = module.exports = sequelize.define('medications', {
    name: {
        type: Sequelize.STRING,
        unique: false,
        allowNull: false
    },
    side_effects_list: {
        type: Sequelize.STRING,
        unique: false,
        allowNull: false
    },
    dosage: {
        type: Sequelize.DOUBLE,
        unique: false,
        allowNull: false
    }
});
