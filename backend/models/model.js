var Sequelize = require('sequelize')
var config = require('../config.json')
var sequelize = (module.exports.sequelize = new Sequelize(config.database, {
  logging: false
}))
var Model = (module.exports.Model = {})

require('fs')
  .readdirSync(__dirname + '/schemas/')
  .forEach(function (file) {
    if (file.match(/\.js$/) !== null) {
      var name = file.replace('.js', '')
      name = name.charAt(0).toUpperCase() + name.slice(1)
      Model[name] = require('./schemas/' + file)
    }
  })

Model.Patient.belongsTo(Model.User, { foreignKey: 'user_id', targetKey: 'id' })
Model.Caregiver.belongsTo(Model.User, {
  foreignKey: 'user_id',
  targetKey: 'id'
})
Model.Caregiver.hasMany(Model.Patient, { as: 'Patients' })

Model.Patient.hasMany(Model.MedicationPlan, { as: 'medical_records' })
Model.MedicationPlan.belongsTo(Model.Patient)

Model.MedPlanItem.belongsTo(Model.MedicationPlan)
Model.MedicationPlan.hasMany(Model.MedPlanItem, { as: 'items' })
Model.MedPlanItem.belongsTo(Model.Medication, { foreignKey: 'medications' })

Model.Activity.belongsTo(Model.Patient)
Model.Notification.belongsTo(Model.User)

sequelize.sync().catch(error => console.log('This error occured', error))
