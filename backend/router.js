var express = require('express');
var router = module.exports = express.Router();
require('fs').readdirSync(__dirname + '/routes/').forEach(function(file) {
    if (file.match(/\.js$/) !== null) {
      router.use(require('./new_routes/' + file));
    }
  });
  