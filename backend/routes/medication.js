var express = require('express');
var router = module.exports = express.Router();
var Middleware = require("../dependencies/middleware_interface.js");
var Controller = require("../dependencies/controller_interface.js")


router.post('/medication',Controller.medication.post);
router.get('/medication',Controller.medication.get);
