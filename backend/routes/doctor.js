var express = require('express');
var router = module.exports = express.Router();
var Middleware = require("../dependencies/middleware_interface.js");
var Controller = require("../dependencies/controller_interface.js")

// router.post('/doctord',Middleware.doctor.caregiver_check, Controller.doctor.assign);
router.post('/setplan', Controller.doctor.addMedicationPlan);
router.get('/getplan', Controller.doctor.getMedicationPlan);
