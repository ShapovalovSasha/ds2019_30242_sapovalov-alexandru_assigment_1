var express = require('express');
var router = module.exports = express.Router();
var Middleware = require("../dependencies/middleware_interface.js");
var Controller = require("../dependencies/controller_interface.js")

router.post('/doctor',Middleware.caregiver.caregiver_check, Controller.caregiver.assign);
router.get('/assigned', Controller.caregiver.get_assigned_patients)
router.post('/assigned', Controller.caregiver.assign)
