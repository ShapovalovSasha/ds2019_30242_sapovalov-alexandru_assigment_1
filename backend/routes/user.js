var express = require('express')
var router = (module.exports = express.Router())
var Middleware = require('../dependencies/middleware_interface.js')
var Controller = require('../dependencies/controller_interface.js')

router.post('/login', Middleware.user.login, Controller.user.login)

router.get('/users', Middleware.user.logged, Controller.user.get)
router.get('/freeusers', Middleware.user.logged, Controller.user.getFree)

router.get('/checkLogin', Middleware.user.logged, Controller.user.check_login)

router.get(
  '/caregivers',
  Middleware.user.logged,
  Middleware.user.doctor,
  Controller.caregiver.get
)

router.post(
  '/users',
  Middleware.user.logged,
  Middleware.user.doctor,
  Controller.user.post
)
router.delete(
  '/users',
  Middleware.user.logged,
  Middleware.user.doctor,
  Controller.user.delete
)

router.get('/updateuser', Middleware.user.logged, Controller.user.update_get)

router.get(
  '/get_new_notifications',
  Middleware.user.logged,
  Controller.notification.get_new_notifications
)
