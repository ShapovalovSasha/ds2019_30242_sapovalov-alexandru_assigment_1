require('fs').readdirSync(require('path').join(__dirname , '../','/controllers/')).forEach(function(file) {
    if (file.match(/\.js$/) !== null) {
      var name = file.replace('.js', '');
      exports[name] = require('../controllers/' + file);
    }
  });
  