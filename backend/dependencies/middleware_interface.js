require('fs').readdirSync(require('path').join(__dirname , '../','/middlewares/')).forEach(function(file) {
    if (file.match(/\.js$/) !== null) {
      var name = file.replace('.js', '');
      exports[name] = require('../middlewares/' + file);
    }
  });
  