var Model = require('../models/model.js').Model
var Sequelize = require('sequelize')
var Op = Sequelize.Op
var patient = (module.exports = {})
var io = require('../app.js')
var controller = require('../controllers/notification.js')
patient.checkActivity = function (req, next) {
  return new Promise((resolve, reject) => {
    Model.Patient.findAll({
      attributes: ['id', 'caregiverId']
    }).then(result => {
      req.body.patient = result[Math.floor(Math.random() * result.length)]

      checkActivityRules(req.body)
      next(req).then(res => {
        resolve(res)
      })
    })
  })
}

// const selectRandomPatient = async _ => {
//   let list = await Model.Patient.findAll({
//     attributes: ['id']
//   }).then(result => {
//     return result
//   })

//   return list[Math.floor(random() * list.length)]
// }

const checkActivityRules = act => {
  activity = JSON.parse(act.activity)
  patient = act.patient
  let timeDiff = diff_hours(new Date(activity.start), new Date(activity.end))
  var rule = {
    rule: '',
    time: 0
  }

  activity.activity === 'Sleeping' && timeDiff < 12
    ? emmitNotification(`a dormit ${timeDiff} ore`, patient.id)
    : (activity.activity === 'Showering' ||
        activity.activity === 'Toileting') &&
      timeDiff > 1
    ? emmitNotification(`a petrecut in baie ${timeDiff} ore`, patient.id)
    : activity.activity === 'Grooming' && timeDiff > 12
    ? emmitNotification(`a lipsit  ${timeDiff} ore`, patient.id)
    : console.log('all good')
}

function diff_hours (dt2, dt1) {
  var diff = (dt2.getTime() - dt1.getTime()) / 1000
  diff /= 60 * 60
  return Math.abs(Math.round(diff))
}

function emmitNotification (message, patient) {
  Model.Patient.findOne({ where: { id: patient } }).then(result => {
    result
      .getUser({
        attributes: [
          'id',
          'username',
          'name',
          'birth_date',
          'gender',
          'address',
          'role'
        ]
      })
      .then(user => {
        const payload = `Pacientul ${user.name} ${message}`
        console.log(payload)
        Model.Caregiver.findOne({
          where: {
            id: result.caregiverId
          }
        }).then(crgv => {
          crgv
            .getUser({
              attributes: ['id']
            })
            .then(user => {
              controller.add_notification(payload, user)
              io.socket.emit('notifications' + user.id, payload)
            })
        })
      })
  })
}
