var Model = require('../models/model.js').Model;
var Sequelize = require('sequelize');
var Op = Sequelize.Op;
var medication = module.exports = {};


medication.post = function (req,res,next){
    if(req.body.name === undefined || req.body.dossage === undefined)
    return next("Empty field")

    Model.Medication.findOne({
        where: {
            name: req.body.name,
            dossage: req.body.dossage
        }
    }).then(result => {
        if(result) {
            return next("Medication already exists");
        } else {
            return next();
        }
    });
}