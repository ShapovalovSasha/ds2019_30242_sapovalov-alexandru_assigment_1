var Model = require('../models/model.js').Model
var Sequelize = require('sequelize')
var Op = Sequelize.Op
var user = (module.exports = {})

user.logged = function (req, res, next) {
  if (req.headers.token == undefined) {
    return next('You are not logged')
  }
  var token = req.headers.token
  Model.User.findOne({
    where: {
      token: token
    }
  })
    .then(function (user) {
      if (user) {
        req.sessionUser = user
        return next()
      } else {
        return next('You are not logged')
      }
    })
    .catch(function (err) {
      return next(err)
    })
}

user.doctor = function (req, res, next) {
  if (req.headers.token == undefined) {
    return next('You are not logged')
  }
  var token = req.headers.token
  Model.User.findOne({
    where: {
      token: token
    }
  })
    .then(function (user) {
      if (user) {
        if (user.role === 1) {
          req.sessionUser = user
          return next()
        } else {
          return next('You do not have permission')
        }
      } else {
        return next('You are not logged')
      }
    })
    .catch(function (err) {
      return next(err)
    })
}

user.caregiver = function (req, res, next) {
  if (req.headers.token == undefined) {
    return next('You are not logged')
  }
  var token = req.headers.token
  Model.User.findOne({
    where: {
      token: token
    }
  })
    .then(function (user) {
      if (user) {
        if (user.role === 2) {
          req.sessionUser = user
          return next()
        } else {
          return next('You do not have permission')
        }
      } else {
        return next('You are not logged')
      }
    })
    .catch(function (err) {
      return next(err)
    })
}

user.login = function (req, res, next) {
  if (req.body.username == undefined || req.body.password == undefined) {
    return next('Empty field')
  }
  return next()
}

user.sign_up = function (req, res, next) {
  if (req.body.username == undefined || req.body.password == undefined) {
    return next('Empty field')
  }
  Model.User.findOne({
    where: {
      username: req.body.username
    }
  })
    .then(result => {
      if (result) {
        return next('Username in use')
      } else {
        return next()
      }
    })
    .catch(function (err) {
      return next(err)
    })
}
user.assign = function (req, res, next) {
  if (
    req.sessionUser.id == undefined ||
    req.sessionUser.userList == undefined
  ) {
    return next('Empty field')
  }
  Promise.All(req.sessionUser.userList, () => {
    Model.Caregiver.create({})
  })
  Model.Caregiver.create({})
    .then(result => {
      if (result) {
        return next('Username in use')
      } else {
        return next()
      }
    })
    .catch(function (err) {
      return next(err)
    })
}
