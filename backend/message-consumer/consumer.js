var amqp = require('amqplib/callback_api')
var ops = require('./activity-handler')
exports.subscribeToQueue = () => {
  amqp.connect('amqp://localhost', function (error0, connection) {
    if (error0) {
      throw error0
    }
    connection.createChannel(function (error1, channel) {
      if (error1) {
        throw error1
      }

      var queue = 'sensors'

      channel.assertQueue(queue, {
        durable: false
      })

      console.log(
        ' [*] Waiting for messages in %s. To exit press CTRL+C',
        queue
      )

      channel.consume(
        queue,
        function (msg) {
          console.log(' [x] Received %s', msg.content.toString())
          ops.handle_activity({ body: { activity: msg.content.toString() } })
        },
        {
          noAck: true
        }
      )
    })
  })
}
