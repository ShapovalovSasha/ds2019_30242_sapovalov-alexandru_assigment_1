var Middleware = require('../dependencies/middleware_interface.js')
var Controller = require('../dependencies/controller_interface.js')

exports.handle_activity = async function (sensorData) {
  let patient = await Middleware.patient.checkActivity(sensorData, saveData)
  console.log(patient)
}

const saveData = async patient => {
  await Controller.patient.saveActivity(patient)
}
