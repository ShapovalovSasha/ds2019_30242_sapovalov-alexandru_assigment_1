var bodyParser = require('body-parser')
var cors = require('cors')
var express = require('express')
var io = (module.exports.socket = require('socket.io')())
var app = express()
var morgan = require('morgan')
var consumer = require('./message-consumer/consumer')

app.set('port', 9000)
app.use(cors())
app.use(morgan('dev'))
app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))
// app.use(cookieParser());

io.listen(2000)
io.sockets.on('connection', function (socket) {
  socket.on('join', function (room) {
    socket.join(room)
  })
})
consumer.subscribeToQueue()
app.use(require('./routes/user.js'))
app.use(require('./routes/medication.js'))
app.use(require('./routes/doctor.js'))
app.use(require('./routes/caregiver.js'))

// ERROR HANDLING
app.use(function errorHandler (err, req, res, next) {
  console.log(err)
  res.status(420).send(err)
})

app.listen(app.get('port'), () =>
  console.log(`App started on port ${app.get('port')}`)
)
