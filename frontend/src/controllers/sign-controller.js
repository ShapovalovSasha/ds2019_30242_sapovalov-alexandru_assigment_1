import { login } from "../api/api_routes.js";

export const  signIn = async payload => {
 let status =  login(payload).then(res => {
    if (res.status === 200) {
      res.text().then(result => {
        const response = JSON.parse(result);
        sessionStorage.setItem("authToken", response.token);
        sessionStorage.setItem("username", payload.username);
        sessionStorage.setItem("role", response.role);
        window.location.assign("/");
        return " "
      });
    } else {
      return "Wrong password or username"
    }
  });
  return await status;
};
