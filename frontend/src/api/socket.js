import socketIOClient from 'socket.io-client'
// socket.on('join', 1)

function subscribeToNotifications (user, callback) {
  const socket = socketIOClient('http://localhost:2000')

  socket.on('notifications' + user, data => {
    console.log(data)
    callback(data)
  })
}

export { subscribeToNotifications }
