import { request } from './request.js'

const url = 'http://localhost:9000/'

//////////
/// All
/////////

export const login = payload => {
  return request(url + 'login/', { body: JSON.stringify(payload) })
}

export const checkLogin = () => {
  return request(url + 'checkLogin/', { method: 'GET' })
}

export const getMyProfile = () => {
  return request(url + 'updateuser/', { method: 'GET' })
}

//////////
/// Patients
/////////

export const getUsers = () => {
  return request(url + 'users/', { method: 'GET' })
}

export const getFreeUsers = () => {
  return request(url + 'freeusers/', { method: 'GET' })
}

//////////
/// Caregivers
/////////
export const getCaregivers = () => {
  return request(url + 'caregivers/', { method: 'GET' })
}

export const getAssignedPatients = payload => {
  return request(url + `assigned?caregiver=${payload.id}`, { method: 'GET' })
}
export const getMedicationPlans = payload => {
  return request(url + `getplan?patientId=${payload.patientId}`, {
    method: 'GET'
  })
}

export const assignPatients = payload => {
  return request(url + 'assigned/', { body: JSON.stringify(payload) })
}

//////////
/// Doctor
/////////

export const addUser = payload => {
  return request(url + 'users/', { body: JSON.stringify(payload) })
}

export const setMedicationPlan = payload => {
  return request(url + 'setplan/', { body: JSON.stringify(payload) })
}

//////////
/// Medications
/////////

export const getMedications = () => {
  return request(url + 'medication/', { method: 'GET' })
}

export const addMedications = payload => {
  return request(url + 'medication/', { body: JSON.stringify(payload) })
}

export const getNewNotifications = payload => {
  return request(url + 'get_new_notifications/', { method: 'GET' })
}
