import React from 'react'
import ReactDOM from 'react-dom'
import { createBrowserHistory } from 'history'
import { Router, Route, Switch, Redirect } from 'react-router-dom'
import { checkLogin } from './api/api_routes.js'
import 'assets/scss/material-kit-react.scss?v=1.8.0'
import ReactNotification from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'

// pages for this product
// import Components from "views/Components/Components.js";
import ProfilePage from 'views/ProfilePage/ProfilePage.js'
import LoginPage from 'views/LoginPage/LoginPage.js'

var hist = createBrowserHistory()
async function logged () {
  return await checkLogin().then(res => {
    if (res.status === 200 || res.status === 304) {
      Promise.resolve(1)
    } else Promise.resolve(0)
  })
}

ReactDOM.render(
  <Router history={hist}>
    <ReactNotification />

    <Switch>
      <Route exact path='/'>
        {!logged() ? <Redirect to='/login-page' /> : <ProfilePage />}
      </Route>
      <Route path='/login-page' component={LoginPage} />
    </Switch>
  </Router>,
  document.getElementById('root')
)
