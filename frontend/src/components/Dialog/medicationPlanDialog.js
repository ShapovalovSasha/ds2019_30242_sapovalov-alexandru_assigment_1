import React, { Fragment } from "react";
// material-ui components
import Slide from "@material-ui/core/Slide";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
// @material-ui/icons
import Close from "@material-ui/icons/Close";
// core components
import Button from "components/CustomButtons/Button.js";
import Add from "@material-ui/icons/Add";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import { getMedications, getUsers } from "../../api/api_routes";
import modalStyle from "../../assets/jss/material-kit-react/modalStyle";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Divider from "@material-ui/core/Divider";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import DeleteIcon from "@material-ui/icons/Delete";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});
const useStyles = makeStyles(modalStyle);

export default function Modal(props) {
  const [modal, setModal] = React.useState(false);
  const [planItems, setPlanItems] = React.useState([]);
  const [planItem, setPlanItem] = React.useState({});
  const [intake, setIntake] = React.useState("");

  const [startingDate, handleStartingDate] = React.useState(new Date());
  const [endingDate, handleEndingDate] = React.useState(new Date());

  const [state, setState] = React.useState({
    patientList: [],
    medicationList: []
  });
  React.useEffect(() => {
    getMedications().then(res => {
      if (res) {
        res.text().then(raw => {
          let med = JSON.parse(raw);
          setState(prevState => {
            return { ...prevState, medicationList: med };
          });
        });
      }
    });
    getUsers().then(res => {
      if (res) {
        res.text().then(raw => {
          let pat = JSON.parse(raw);
          setState(prevState => {
            return { ...prevState, patientList: pat };
          });
        });
      }
    });
  }, []);
  const classes = useStyles();
  return (
    <div>
      {/* <Button color="rose" round onClick={() => setModal(true)}>
        Modal
      </Button> */}
      <Button color="success" round justIcon onClick={() => setModal(true)}>
        <Add />
      </Button>
      <Dialog
        classes={{
          root: classes.center,
          paper: classes.modal
        }}
        open={modal}
        TransitionComponent={Transition}
        keepMounted
        onClose={() => setModal(false)}
        aria-labelledby="modal-slide-title"
        aria-describedby="modal-slide-description"
      >
        <DialogTitle
          id="classic-modal-slide-title"
          disableTypography
          className={classes.modalHeader}
        >
          <IconButton
            className={classes.modalCloseButton}
            key="close"
            aria-label="Close"
            color="inherit"
            onClick={() => setModal(false)}
          >
            <Close className={classes.modalClose} />
          </IconButton>
          <h4 className={classes.modalTitle}>Create new medical plan</h4>
        </DialogTitle>
        <DialogContent
          id="modal-slide-description"
          className={classes.modalBody}
        >
          <h5>Fill inputs and add items to plan</h5>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              marginBottom: 30
            }}
          >
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <DatePicker
                disableToolbar
                // variant="inline"
                label="Starting Date"
                value={startingDate}
                onChange={handleStartingDate}
              />
            </MuiPickersUtilsProvider>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <DatePicker
                disableToolbar
                // variant="inline"
                label="Ending Date"
                value={endingDate}
                onChange={handleEndingDate}
              />
            </MuiPickersUtilsProvider>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              marginBottom: 30
            }}
          >
            <Autocomplete
              options={state.medicationList}
              getOptionLabel={option => option.name}
              style={{ width: 300 }}
            //   value={planItem.medication}
              onChange={(event, newValue) => {
                setPlanItem(newValue);
                console.log(newValue);
              }}
              renderInput={params => (
                <TextField
                  {...params}
                //   value={plan}
                  InputLabelProps={{ shrink: true }}
                  label="Select Medication"
                  variant="standard"
                  fullWidth
                />
              )}
            />
            <TextField
              InputLabelProps={{ shrink: true }}
              label="Intake interval"
              variant="standard"
              style={{ width: 300, marginLeft: 20 }}
              value= {intake}
              onChange={ (e) => {
                setIntake(e.target.value)
              }}
            />
          </div>
          <Button
            color="info"
            onClick={() => {
              let plan = planItems;
              let item = planItem;
              plan.push({
                  medication:item,
                  interval:intake
              });
              setPlanItems(plan);
              setPlanItem(null)
              setIntake('')
            }}
          >
            Add item to plan
          </Button>
          <Divider />
          <List dense>
            {planItems.map((item, index) => (
              <ListItem key={index}>
                <ListItemText primary={`${item.medication.name} => ${item.interval}`} />
                <ListItemSecondaryAction>
                  <IconButton edge="end" aria-label="delete">
                    <DeleteIcon />
                  </IconButton>
                </ListItemSecondaryAction>
              </ListItem>
            ))}
          </List>
        </DialogContent>
        <DialogActions
          className={classes.modalFooter + " " + classes.modalFooterCenter}
        >
          <Button onClick={() => {
              props.setPlan({
                  user_id:props.patient,
                  starting_date: startingDate,
                  ending_date : endingDate,
                  medplan : planItems
              })
              setModal(false)}} color="success">
            Create plan
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
