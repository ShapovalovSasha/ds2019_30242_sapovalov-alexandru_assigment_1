import React from 'react'
// nodejs library that concatenates classes
import classNames from 'classnames'
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles'
// @material-ui/icons
import Person from '@material-ui/icons/Person'
import Palette from '@material-ui/icons/AddCircle'
import Favorite from '@material-ui/icons/Colorize'
// core components
import CircularProgress from '@material-ui/core/CircularProgress'
import Footer from 'components/Footer/Footer.js'
import GridContainer from 'components/Grid/GridContainer.js'
import GridItem from 'components/Grid/GridItem.js'
import NavPills from 'components/NavPills/NavPills.js'
import Parallax from 'components/Parallax/Parallax.js'
import Header from 'components/Header/Header.js'
import HeaderLinks from 'components/Header/HeaderLinks.js'
import socketIOClient from 'socket.io-client'
import { store } from 'react-notifications-component'

import profile from 'assets/img/faces/doctor.png'
import styles from 'assets/jss/material-kit-react/views/profilePage.js'
import PatientsManager from './Scenes/patientsManager.js'
import PatientPage from './Scenes/patientPage.js'
import CaregiverManager from './Scenes/caregiverManager'
import MedicationManager from './Scenes/medicationManager'
import CaregiverPage from './Scenes/caregiverPage'
import { subscribeToNotifications } from '../../api/socket'
import {
  getMyProfile,
  checkLogin,
  getNewNotifications
} from '../../api/api_routes'
import { stat } from 'fs'
const useStyles = makeStyles(styles)
const role = ['Patient', 'Doctor', 'Caregiver']
export default function ProfilePage (props) {
  const [load, setLoad] = React.useState(true)
  // const [notifications, setNotifications] = React.useState([])
  const [state, setState] = React.useState({
    name: '',
    role: 0,
    id: -1
  })
  // React.useEffect(() => {
  //   notifications.map(element => {
  //     store.addNotification({
  //       title: 'Hey!',
  //       message: element.message,
  //       type: 'warning',
  //       insert: 'top',
  //       container: 'top-right',
  //       animationIn: ['animated', 'fadeIn'],
  //       animationOut: ['animated', 'fadeOut'],
  //       dismiss: {
  //         duration: 0,
  //         onScreen: true,
  //         click: true
  //       }
  //     })
  //   })
  // }, [notifications])
  React.useEffect(() => {
    checkLogin().then(res => {
      if (res.status === 200 || res.status === 304) {
        setLoad(false)
      } else window.location.assign('/login-page')
    })
    getMyProfile().then(res => {
      if (res)
        res.text().then(result => {
          const data = JSON.parse(result)
          // if (data[0].role === 2)
          //   subscribeToNotifications(data[0], a => {
          //     console.log(a)
          //   })
          setState({ name: data[0].name, role: data[0].role, id: data[0].id })
        })
    })
    getNewNotifications().then(res => {
      res.text().then(raw => {
        const notifications = JSON.parse(raw)
        notifications.forEach(element => {
          store.addNotification({
            title: 'Hey!',
            message: element.message,
            type: 'warning',
            insert: 'top',
            container: 'top-right',
            animationIn: ['animated', 'fadeIn'],
            animationOut: ['animated', 'fadeOut'],
            dismiss: {
              duration: 0,
              onScreen: true,
              click: true
            }
          })
        })
      })
    })
  }, [])
  const classes = useStyles()
  const { ...rest } = props
  const imageClasses = classNames(
    classes.imgRaised,
    classes.imgRoundedCircle,
    classes.imgFluid
  )
  const socket = socketIOClient('http://localhost:2000')

  socket.on('notifications' + state.id, data => {
    getNewNotifications().then(res => {
      res.text().then(raw => {
        const notifications = JSON.parse(raw)
        notifications.forEach(element => {
          store.addNotification({
            title: 'Hey!',
            message: element.message,
            type: 'warning',
            insert: 'top',
            container: 'top-right',
            animationIn: ['animated', 'fadeIn'],
            animationOut: ['animated', 'fadeOut'],
            dismiss: {
              duration: 0,
              onScreen: true,
              click: true
            }
          })
        })
      })
    })
  })
  const navImageClasses = classNames(classes.imgRounded, classes.imgGallery)
  if (load)
    return (
      <GridContainer justify='center'>
        <CircularProgress
          style={{ marginTop: '100%' }}
          className={classes.center}
        />
      </GridContainer>
    )
  else
    return (
      <div>
        <Header
          color='transparent'
          brand='Med &#9877; Care'
          rightLinks={<HeaderLinks />}
          fixed
          changeColorOnScroll={{
            height: 200,
            color: 'white'
          }}
          {...rest}
        />
        <Parallax small filter image={require('assets/img/profile-bg.jpg')} />
        <div className={classNames(classes.main, classes.mainRaised)}>
          <div>
            <div className={classes.container}>
              <GridContainer justify='center'>
                <GridItem xs={12} sm={12} md={6}>
                  <div className={classes.profile}>
                    <div>
                      <img src={profile} alt='...' className={imageClasses} />
                    </div>
                    <div className={classes.name}>
                      <h3 className={classes.title}>{state.name}</h3>
                      <h6>{role[state.role]}</h6>
                    </div>
                  </div>
                </GridItem>
              </GridContainer>
              <GridContainer justify='center'>
                <GridItem
                  xs={12}
                  sm={12}
                  md={12}
                  className={classes.navWrapper}
                >
                  <NavPills
                    alignCenter
                    color='warning'
                    tabs={
                      state.role === 1
                        ? [
                            {
                              tabButton: 'Patients',
                              tabIcon: Person,
                              tabContent: (
                                <GridContainer justify='center'>
                                  <GridItem xs={12} sm={12} md={12}>
                                    <PatientsManager />
                                  </GridItem>
                                </GridContainer>
                              )
                            },
                            {
                              tabButton: 'Caregivers',
                              tabIcon: Palette,
                              tabContent: (
                                <GridContainer justify='center'>
                                  <GridItem xs={12} sm={12} md={12}>
                                    <CaregiverManager />
                                  </GridItem>
                                </GridContainer>
                              )
                            },
                            {
                              tabButton: 'Medications',
                              tabIcon: Favorite,
                              tabContent: (
                                <GridContainer justify='center'>
                                  <GridItem xs={12} sm={12} md={12}>
                                    <MedicationManager />
                                  </GridItem>
                                </GridContainer>
                              )
                            }
                          ]
                        : state.role === 0
                        ? [
                            {
                              tabButton: 'My medication plans',
                              tabIcon: Favorite,
                              tabContent: (
                                <GridContainer justify='center'>
                                  <GridItem xs={12} sm={12} md={12}>
                                    <PatientPage user_id={state.id} />
                                  </GridItem>
                                </GridContainer>
                              )
                            }
                          ]
                        : state.role === 2 && [
                            {
                              tabButton: 'My pacients',
                              tabIcon: Favorite,
                              tabContent: (
                                <GridContainer justify='center'>
                                  <GridItem xs={12} sm={12} md={12}>
                                    <CaregiverPage user_id={state.id} />
                                  </GridItem>
                                </GridContainer>
                              )
                            }
                          ]
                    }
                  />
                </GridItem>
              </GridContainer>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    )
}
