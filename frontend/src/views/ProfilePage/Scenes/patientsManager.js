import React, { Fragment } from 'react'
import MaterialTable from 'material-table'
import TextField from '@material-ui/core/TextField'
import Divider from '@material-ui/core/Divider'
import Modal from '../../../components/Dialog/medicationPlanDialog'
import {
  getUsers,
  addUser,
  getMedicationPlans,
  setMedicationPlan
} from '../../../api/api_routes.js'
import Card from 'components/Card/Card.js'
import CardBody from 'components/Card/CardBody.js'
import Button from 'components/CustomButtons/Button.js'
import { makeStyles } from '@material-ui/core/styles'
import { cardTitle } from 'assets/jss/material-kit-react.js'
const mocks = [
  {
    password: '123123',
    username: 'pac1',
    name: 'Martena Walker',
    birth_date: '2019-04-23 23:00:38',
    gender: 0,
    address: '451-4107 Ipsum Rd.',
    role: 0
  },
  {
    password: '123123',
    username: 'pac2',
    name: 'Evelyn May',
    birth_date: '2020-02-17 15:11:05',
    gender: 1,
    address: '7110 Lectus Ave',
    role: 0
  },
  {
    password: '123123',
    username: 'pac3',
    name: 'Brenden Pope',
    birth_date: '2020-04-22 04:01:19',
    gender: 0,
    address: 'Ap #923-2953 Proin Ave',
    role: 0
  },
  {
    password: '123123',
    username: 'pac4',
    name: 'Howard Crawford',
    birth_date: '2019-06-14 02:55:47',
    gender: 0,
    address: '8427 Eros Ave',
    role: 0
  },
  {
    password: '123123',
    username: 'pac5',
    name: 'Claire Howard',
    birth_date: '2019-07-10 21:26:05',
    gender: 1,
    address: 'Ap #519-9368 Ac Rd.',
    role: 0
  },
  {
    password: '123123',
    username: 'pac6',
    name: 'Quynn Whitley',
    birth_date: '2019-03-13 15:33:39',
    gender: 1,
    address: 'P.O. Box 974, 2211 Sed Ave',
    role: 0
  },
  {
    password: '123123',
    username: 'pac7',
    name: 'Quyn Mcdaniel',
    birth_date: '2019-10-12 05:38:07',
    gender: 1,
    address: '104-4723 Aliquam St.',
    role: 0
  },
  {
    password: '123123',
    username: 'pac8',
    name: 'Daphne Price',
    birth_date: '2020-05-24 06:11:25',
    gender: 0,
    address: '9955 Magnis Road',
    role: 0
  },
  {
    password: '123123',
    username: 'pac9',
    name: 'May Vazquez',
    birth_date: '2019-12-02 15:00:16',
    gender: 1,
    address: 'Ap #705-8980 Enim. Av.',
    role: 0
  },
  {
    password: '123123',
    username: 'pac10',
    name: 'Theodore Vargas',
    birth_date: '2020-12-20 09:45:22',
    gender: 1,
    address: '6928 Quis Road',
    role: 0
  }
]
const gender = ['Female', 'Male']
const role = ['Patient', 'Doctor', 'Caregiver']
const styles = {
  cardTitle,
  textCenter: {
    textAlign: 'center'
  },
  textRight: {
    textAlign: 'right'
  },
  detailsBox: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '10',
    flexWrap: 'wrap'
  },
  planContent: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  detailsHeader: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    margin: 10
  }
}

const useStyles = makeStyles(styles)

const PatientsManager = () => {
  const classes = useStyles()

  const [state, setState] = React.useState({
    patients: []
  })
  React.useEffect(() => {
    // mocks.forEach(newData => {
    //   addUser({ user: newData })
    // })
    getUsers().then(res => {
      res.text().then(result => {
        if (result) {
          const data = JSON.parse(result)
          var medicationPlan = []
          for (let item of data) {
            getMedicationPlans({ patientId: parseInt(item.user_id) }).then(
              res => {
                res.text().then(raw => {
                  var patientsList = JSON.parse(raw)
                  item.medicationPlan = patientsList
                  medicationPlan.push(patientsList)
                })
              }
            )
          }
          data.forEach(element => {
            element.user.password = ''
          })

          setState(prevState => {
            return { ...prevState, patients: data }
          })
        }
      })
    })
  }, [])

  return (
    <MaterialTable
      title='Manage patients'
      options={{
        actionsColumnIndex: -1
      }}
      columns={[
        { title: 'Username', field: 'user.username', editable: 'onAdd' },
        {
          title: 'Password',
          field: 'user.password',
          editable: 'onAdd',
          editComponent: props => (
            <TextField
              onChange={e => props.onChange(e.target.value)}
              type='password'
            />
          )
        },
        { title: 'Name', field: 'user.name' },
        { title: 'Address', field: 'user.address' },
        {
          title: 'Gender',
          field: 'user.gender',
          lookup: { 0: gender[0], 1: gender[1] },
          render: rowData => <Fragment>{gender[rowData.user.gender]}</Fragment>
        },
        {
          title: 'Role',
          field: 'user.role',
          lookup: { 0: role[0], 1: role[1], 2: role[2] },
          render: rowData => <Fragment>{role[rowData.user.role]}</Fragment>
        },
        {
          title: 'Birth Date ',
          field: 'user.birth_date',
          type: 'date',
          render: rowData => (
            <Fragment>
              {new Date(rowData.user.birth_date).toLocaleDateString()}
            </Fragment>
          )
        }
      ]}
      data={state.patients}
      editable={{
        onRowAdd: newData =>
          new Promise((resolve, reject) => {
            addUser(newData).then(res => {
              if (res) {
                if (res.status === 200) {
                  const data = state.patients
                  data.push(newData)
                  setState({ patients: data })
                  resolve()
                }
              }
            })
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              {
                const data = this.state.data
                const index = data.indexOf(oldData)
                data[index] = newData
                this.setState({ data }, () => resolve())
              }
              resolve()
            }, 1000)
          }),
        onRowDelete: oldData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              {
                let data = this.state.data
                const index = data.indexOf(oldData)
                data.splice(index, 1)
                this.setState({ data }, () => resolve())
              }
              resolve()
            }, 1000)
          })
      }}
      detailPanel={rowData => {
        const row = rowData.medicationPlan
        return (
          <div>
            <div className={classes.detailsHeader}>
              <Modal
                patient={rowData.user_id}
                setPlan={payload => {
                  setMedicationPlan(payload).then(res => {
                    if (res.status === 200) {
                      return true
                    } else return false
                  })
                }}
              />
            </div>
            <div className={classes.detailsBox}>
              {row.map((item, key) => (
                <Card
                  style={{
                    width: '80%',
                    margin: '20px 50px',
                    maxWidth: '350px',
                    minWidth: '340px'
                  }}
                  key={key}
                >
                  <CardBody>
                    <h6 className={classes.cardSubtitlecardTitle}>{`${new Date(
                      item.starting_date
                    ).toLocaleDateString()} - ${new Date(
                      item.ending_date
                    ).toLocaleDateString()}`}</h6>
                    {item.items.map((planitem, key) => (
                      <Fragment key={key}>
                        <Divider variant='middle' />
                        <h4 className={classes.cardTitle}>
                          {item.items[key].medication.name}
                        </h4>
                        <div className={classes.planContent}>
                          <p>
                            <small className={classes.textMuted}>
                              Dosage(mg.)
                            </small>
                          </p>
                          <p>{planitem.medication.dosage}</p>
                        </div>
                        <div className={classes.planContent}>
                          <p>
                            <small className={classes.textMuted}>
                              Intake interval:
                            </small>
                          </p>
                          <p>{planitem.intake_interval}</p>
                        </div>
                        <div className={classes.planContent}>
                          <p>
                            <small className={classes.textMuted}>
                              Side effects
                            </small>
                          </p>
                          <p>{planitem.medication.side_effects_list}</p>
                        </div>
                      </Fragment>
                    ))}
                  </CardBody>
                </Card>
              ))}
            </div>
          </div>
        )
      }}
    />
  )
}

export default PatientsManager
