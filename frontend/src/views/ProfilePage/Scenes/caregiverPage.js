import React, { Fragment } from "react";
import MaterialTable from "material-table";
import TextField from "@material-ui/core/TextField";
import Divider from "@material-ui/core/Divider";
import Modal from "../../../components/Dialog/medicationPlanDialog";
import {
  getUsers,
  addUser,
  getMedicationPlans,
  getAssignedPatients
} from "../../../api/api_routes.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import Button from "components/CustomButtons/Button.js";
import { makeStyles } from "@material-ui/core/styles";
import { cardTitle } from "assets/jss/material-kit-react.js";
const gender = ["Female", "Male"];
const role = ["Patient", "Doctor", "Caregiver"];
const styles = {
  cardTitle,
  textCenter: {
    textAlign: "center"
  },
  textRight: {
    textAlign: "right"
  },
  detailsBox: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    padding: "10"
  },
  planContent: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  detailsHeader: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-end",
    margin: 10
  }
};

const useStyles = makeStyles(styles);

const CaregiverPage = (props) => {
  const classes = useStyles();

  const [state, setState] = React.useState({
    patients: []
  });
  React.useEffect(() => {
    getAssignedPatients({ id: parseInt(props.user_id) }).then(res => {
      res.text().then(result => {
        if (result) {
          const data = JSON.parse(result);
          for (let item of data) {
            getMedicationPlans({ patientId: parseInt(item.user_id) }).then(
              res => {
                res.text().then(raw => {
                  var patientsList = JSON.parse(raw);
                  item.medicationPlan = patientsList;
                });
              }
            );
          }
          data.forEach(element => {
            element.user.password = "";
          });

          setState(prevState => {
            return { ...prevState, patients: data };
          });
        }
      });
    });
  }, []);

  return (
    <MaterialTable
      title="Manage patients"
      options={{
        actionsColumnIndex: -1
      }}
      columns={[
        { title: "Username", field: "user.username", editable: "onAdd" },
        {
          title: "Password",
          field: "user.password",
          editable: "onAdd",
          editComponent: props => (
            <TextField
              onChange={e => props.onChange(e.target.value)}
              type="password"
            />
          )
        },
        { title: "Name", field: "user.name" },
        { title: "Address", field: "user.address" },
        {
          title: "Gender",
          field: "user.gender",
          lookup: { 0: gender[0], 1: gender[1] },
          render: rowData => <Fragment>{gender[rowData.user.gender]}</Fragment>
        },
        {
          title: "Role",
          field: "user.role",
          lookup: { 0: role[0], 1: role[1], 2: role[2] },
          render: rowData => <Fragment>{role[rowData.user.role]}</Fragment>
        },
        {
          title: "Birth Date ",
          field: "user.birth_date",
          type: "date",
          render: rowData => (
            <Fragment>
              {new Date(rowData.user.birth_date).toLocaleDateString()}
            </Fragment>
          )
        }
      ]}
      data={state.patients}
      detailPanel={rowData => {
        const row = rowData.medicationPlan;
        return (
          <div>
            <div className={classes.detailsBox}>
              {row.map((item, key) => (
                <Card style={{ width: "80%", margin: "20px 50px" }} key={key}>
                  <CardBody>
                    <h6 className={classes.cardSubtitlecardTitle}>{`${new Date(
                      item.starting_date
                    ).toLocaleDateString()} - ${new Date(
                      item.ending_date
                    ).toLocaleDateString()}`}</h6>
                    {item.items.map((planitem, key) => (
                      <Fragment key={key}>
                        <Divider variant="middle" />
                        <h4 className={classes.cardTitle}>
                          {item.items[key].medication.name}
                        </h4>
                        <div className={classes.planContent}>
                          <p>
                            <small className={classes.textMuted}>
                              Dosage(mg.)
                            </small>
                          </p>
                          <p>{planitem.medication.dosage}</p>
                        </div>
                        <div className={classes.planContent}>
                          <p>
                            <small className={classes.textMuted}>
                              Intake interval:
                            </small>
                          </p>
                          <p>{planitem.intake_interval}</p>
                        </div>
                        <div className={classes.planContent}>
                          <p>
                            <small className={classes.textMuted}>
                              Side effects
                            </small>
                          </p>
                          <p>{planitem.medication.side_effects_list}</p>
                        </div>
                      </Fragment>
                    ))}
                  </CardBody>
                </Card>
              ))}
            </div>
          </div>
        );
      }}
    />
  );
};

export default CaregiverPage;
