import React, { Fragment } from "react";
import MaterialTable from "material-table";
import TextField from "@material-ui/core/TextField";
import Divider from "@material-ui/core/Divider";
import Modal from "../../../components/Dialog/medicationPlanDialog";
import {
  getUsers,
  addUser,
  getMedicationPlans,
  setMedicationPlan
} from "../../../api/api_routes.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import Button from "components/CustomButtons/Button.js";
import { makeStyles } from "@material-ui/core/styles";
import { cardTitle } from "assets/jss/material-kit-react.js";
const gender = ["Female", "Male"];
const role = ["Patient", "Doctor", "Caregiver"];
const styles = {
  cardTitle,
  textCenter: {
    textAlign: "center"
  },
  textRight: {
    textAlign: "right"
  },
  detailsBox: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    padding: "10"
  },
  planContent: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  detailsHeader: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-end",
    margin: 10
  }
};

const useStyles = makeStyles(styles);

const PatientPage = (props) => {
  const classes = useStyles();
  const [plan,setPlan] = React.useState([])
  const [state, setState] = React.useState({
    patients: [],
    medplan:{}
  });
  React.useEffect(() => {
    if(props.user_id!==-1)
            getMedicationPlans({ patientId: parseInt(props.user_id) }).then(
              res => {
                res.text().then(raw => {
                  var patientsList = JSON.parse(raw);
                  setPlan(patientsList);
                });
              }
            );
  }, [props.user_id]);

  return (
    <div className={classes.detailsBox}>
    {plan.map((item, key) => (
      <Card style={{ width: "80%", margin: "20px 50px" }} key={key}>
        <CardBody>
          <h6 className={classes.cardSubtitlecardTitle}>{`${new Date(
            item.starting_date
          ).toLocaleDateString()} - ${new Date(
            item.ending_date
          ).toLocaleDateString()}`}</h6>
          {item.items.map((planitem, key) => (
            <Fragment key={key}>
              <Divider variant="middle" />
              <h4 className={classes.cardTitle}>
                {item.items[key].medication.name}
              </h4>
              <div className={classes.planContent}>
                <p>
                  <small className={classes.textMuted}>
                    Dosage(mg.)
                  </small>
                </p>
                <p>{planitem.medication.dosage}</p>
              </div>
              <div className={classes.planContent}>
                <p>
                  <small className={classes.textMuted}>
                    Intake interval:
                  </small>
                </p>
                <p>{planitem.intake_interval}</p>
              </div>
              <div className={classes.planContent}>
                <p>
                  <small className={classes.textMuted}>
                    Side effects
                  </small>
                </p>
                <p>{planitem.medication.side_effects_list}</p>
              </div>
            </Fragment>
          ))}
        </CardBody>
      </Card>
    ))}
  </div>
  );
};

export default PatientPage;
