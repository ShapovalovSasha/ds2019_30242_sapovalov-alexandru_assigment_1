import React, { Fragment } from 'react'
import MaterialTable from 'material-table';
import TextField from "@material-ui/core/TextField";
import {getMedications,addMedications} from "../../../api/api_routes.js"
const gender = ["Female","Male"]
const role = ["Patient","Doctor","Caregiver"]

const MedicationManager = () => {
    const [state,setState] = React.useState({
        medications : []
    })
    React.useEffect(()=>{
        getMedications().then(res=>{
            res.text().then(result => {
                if(result)
               { 
                   const data = JSON.parse(result);
                setState({...state,medications:data})
            }
            })
        })
    },[])

    return (
        <MaterialTable
        title="Manage medications"
        options={{
          actionsColumnIndex: -1
        }}
        columns={[
          { title: 'Name', field: 'name'},
          { title: 'Dosage (mg)', field: 'dosage'},
          { title: 'Side Effects', field: 'side_effects_list' },
        ]}
        data={state.medications}
        editable={{
          onRowAdd: newData =>
            new Promise((resolve, reject) => {
              addMedications(newData).then(res=>{
                  if(res){
                      if(res.status === 200){
                          const data = state.medications;
                          data.push(newData)
                          setState({medications : data})
                          resolve();
                      }
                  }
              })
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                {
                  const data = this.state.data;
                  const index = data.indexOf(oldData);
                  data[index] = newData;
                  this.setState({ data }, () => resolve());
                }
                resolve()
              }, 1000)
            }),
          onRowDelete: oldData =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                {
                  let data = this.state.data;
                  const index = data.indexOf(oldData);
                  data.splice(index, 1);
                  this.setState({ data }, () => resolve());
                }
                resolve()
              }, 1000)
            }),
        }}
      />
    )
}

export default MedicationManager
