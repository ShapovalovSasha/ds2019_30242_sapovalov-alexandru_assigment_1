import React, { Fragment } from "react";
import MaterialTable from "material-table";
import TextField from "@material-ui/core/TextField";
import {
  getCaregivers,
  addUser,
  getUsers,
  getAssignedPatients,
  assignPatients
} from "../../../api/api_routes.js";
import Autocomplete from "../../../components/SelectAutocomplete/autocomplete.js";

import { makeStyles } from "@material-ui/core/styles";
// core components
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import Button from "components/CustomButtons/Button.js";

import { cardTitle } from "assets/jss/material-kit-react.js";

const styles = {
  cardTitle,
  textCenter: {
    textAlign: "center"
  },
  textRight: {
    textAlign: "right"
  },
  detailsBox: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    padding: "10"
  }
};

const gender = ["Female", "Male"];
const role = ["Patient", "Doctor", "Caregiver"];
const useStyles = makeStyles(styles);

const CaregiversManager = () => {
  const classes = useStyles();
  const [addedPatients, addNewPatients] = React.useState([]);
  const [state, setState] = React.useState({
    patients: [],
    caregivers: []
  });
  React.useEffect(() => {
    getCaregivers().then(res => {
      res.text().then(result => {
        if (result) {
          const data = JSON.parse(result);
          let assignedUsers = [];

          for (let item of data) {
            getAssignedPatients({ id: parseInt(item.user_id) }).then(res => {
              res.text().then(raw => {
                var patientsList = JSON.parse(raw);
                item.assigned_patients = patientsList;
                assignedUsers.push(patientsList);
              });
            });
          }
          data.forEach(element => {
            element.user.password = "";
          });

        
          setState(prevState => {
            return { ...prevState, caregivers: data };
          });
        }
      });
    });
    getUsers().then(res => {
      res.text().then(raw => {
        const patients = JSON.parse(raw);
        setState(prevState => {
          return { ...prevState, patients: patients };
        });
      });
    });
  }, []);

  return (
    <MaterialTable
      title="Manage caregivers"
      options={{
          actionsColumnIndex: -1
        }}
      columns={[
        { title: "Username", field: "user.username", editable: "onAdd" },
        {
          title: "Password",
          field: "user.password",
          editable: "onAdd",
          render: rowData => <Fragment>xxxx</Fragment>,
          editComponent: props => (
            <TextField
              onChange={e => props.onChange(e.target.value)}
              type="password"
            />
          )
        },
        { title: "Name", field: "user.name" },
        { title: "Address", field: "user.address" },
        {
          title: "Gender",
          field: "user.gender",
          lookup: { 0: gender[0], 1: gender[1] },
          render: rowData => <Fragment>{gender[rowData.user.gender]}</Fragment>
        },
        {
          title: "Role",
          field: "user.role",
          lookup: { 0: role[0], 1: role[1], 2: role[2] },
          render: rowData => <Fragment>{role[rowData.user.role]}</Fragment>
        },
        {
          title: "Birth Date ",
          field: "user.birth_date",
          type: "date",
          render: rowData => (
            <Fragment>
              {new Date(rowData.user.birth_date).toLocaleDateString()}
            </Fragment>
          )
        }
      ]}
      data={state.caregivers}
      editable={{
        onRowAdd: newData =>
          new Promise((resolve, reject) => {
            addUser(newData).then(res => {
              if (res) {
                if (res.status === 200) {
                  const data = state.caregivers;
                  data.push(newData);
                  setState({ ...state, caregivers: data });
                  resolve();
                }
              }
            });
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              {
                const data = this.state.data;
                const index = data.indexOf(oldData);
                data[index] = newData;
                this.setState({ data }, () => resolve());
              }
              resolve();
            }, 1000);
          }),
        onRowDelete: oldData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              {
                let data = this.state.data;
                const index = data.indexOf(oldData);
                data.splice(index, 1);
                this.setState({ data }, () => resolve());
              }
              resolve();
            }, 1000);
          })
      }}
      detailPanel={rowData => {
        return (
          <div className={classes.detailsBox}>
            <Card style={{ width: "80%" }}>
              <CardBody>
                <h4 className={classes.cardTitle}>Assign patients</h4>
                <p>Here you can assign patients to caregiver</p>
                <Autocomplete
                  addTagInForm={data => {
                    let currentBuffer = addedPatients;
                    let patient_ids = []
                    data.forEach(item=>{
                      patient_ids.push(item.id)
                    })
                    currentBuffer[`id${rowData.user_id}`] = patient_ids;
                    addNewPatients(currentBuffer);
                  }}
                  selectedUsers={rowData.assigned_patients}
                />
                <Button color="success" onClick={() => {
                  if(addedPatients[`id${rowData.user_id}`])
                  assignPatients({caregiverId:rowData.user_id,caregivers_list:addedPatients[`id${rowData.user_id}`]})
                }}>
                  Apply
                </Button>
              </CardBody>
            </Card>
          </div>
        );
      }}
    />
  );
};

export default CaregiversManager;
