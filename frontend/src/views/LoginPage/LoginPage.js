import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/People";
// core components
import Header from "components/Header/Header.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import CustomInput from "components/CustomInput/CustomInput.js";

import styles from "assets/jss/material-kit-react/views/loginPage.js";

import image from "assets/img/login-wall.jpg";
import {signIn} from "../../controllers/sign-controller.js"
const useStyles = makeStyles(styles);

export default function LoginPage(props) {
  const [cardAnimaton, setCardAnimation] = React.useState("cardHidden");
  const [errorMessage,setError] = React.useState("");
  const [state, setState] = React.useState({
    username: "",
    password: ""
  });

  const updateField = e => {
    setState({ ...state, [e.target.name]: e.target.value });
  };

  setTimeout(function() {
    setCardAnimation("");
  }, 700);
  const classes = useStyles();
  const { ...rest } = props;
  return (
    <div>
      <div
        className={classes.pageHeader}
        style={{
          backgroundImage: "url(" + image + ")",
          backgroundSize: "cover",
          backgroundPosition: "top center"
        }}
      >
        <div className={classes.container}>
          <GridContainer justify="center">
            <GridItem xs={12} sm={12} md={4}>
              <Card className={classes[cardAnimaton]}>
                <CardHeader color="info" className={classes.cardHeader}>
                  <h4>Med &#9877; Care</h4>
                </CardHeader>
                <p className={classes.divider}>Log in</p>
                <CardBody>
                  <CustomInput
                    labelText="First Name..."
                    id="first"
                    // onChange = {(e)=>setState({...state,username:e.target.value})}
                    value={state.username}
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      onChange: e => updateField(e),
                      value: state.username,
                      type: "text",
                      name: "username",
                      endAdornment: (
                        <InputAdornment position="end">
                          <People className={classes.inputIconsColor} />
                        </InputAdornment>
                      )
                    }}
                  />
                  <CustomInput
                    labelText="Password"
                    id="pass"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      onKeyUp: (e) => {if (e.key === 'Enter') signIn(state).then(err=>setError(err))},
                      type: "password",
                      onChange: e => updateField(e),
                      value: state.password,
                      name: "password",
                      endAdornment: (
                        <InputAdornment position="end">
                          <Icon className={classes.inputIconsColor}>
                            lock_outline
                          </Icon>
                        </InputAdornment>
                      ),
                      autoComplete: "off"
                    }}
                  />
                </CardBody>
                <div className={classes.center}>
                   <p style ={{textAlign:'center',color:"red"}}> {errorMessage}</p>
                  </div>
                <CardFooter className={classes.cardFooter}>
                  <Button
                    simple
                    color="primary"
                    size="lg"
                    onClick={() => signIn(state).then(err=>setError(err))}
                  >
                    SIGN IN
                  </Button>
                 
                </CardFooter>
              </Card>
            </GridItem>
          </GridContainer>
        </div>
        <Footer whiteFont />
      </div>
    </div>
  );
}
