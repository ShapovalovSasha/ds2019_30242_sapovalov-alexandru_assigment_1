var amqp = require('amqplib/callback_api')

// Connection to RabbitMQ server (running on localhost on standard port (5672)) via amqp
//  Connection string format   'amqp://user:user@whateverhost:whateverport'
const CONN_URL = 'amqp://localhost'
var amqpConn = null

// method for adding new data in queue
exports.publishToQueue = async (queueName, data) => {
  amqp.connect('amqp://localhost', function (error0, connection) {
    if (error0) {
      throw error0
    }
    // creating channel for running connection
    connection.createChannel(function (error1, channel) {
      if (error1) {
        throw error1
      }
      channel.assertQueue(queueName, {
        durable: false
      })

      // channel.publish(queueName, '', Buffer.from(data), { persistent: true })
      channel.sendToQueue(queueName, Buffer.from(data))

      // cleaning up and closing connection
      channel.close(() => {
        console.log(' [x] Sent %s', data)
        connection.close()
      })
    })
  })
}
