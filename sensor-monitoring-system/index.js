const provider = require('./provider')
var fs = require('fs')

// reading data from txt file & saving each line as item in array
const sensors = fs.readFileSync(__dirname + '/activity.txt')
const sensorData = sensors.toString().split('\n')

const sendData = async _ => {
  let i = 0
  for (let element of sensorData) {
    const data = element.split('@')
    const msg = JSON.stringify({
      patient_id: 1,
      activity: data[2],
      start: data[0],
      end: data[1]
    })
    const status = await sendMsg(msg)
    console.log(i++, status)
  }
}

const sendMsg = payload => {
  return new Promise(function (resolve, reject) {
    provider.publishToQueue('sensors', payload).then(res => {
      setTimeout(function () {
        resolve('done')
      }, 100)
    })
  })
}

sendData()
